#version 330

#define KHRONOS

#ifndef MIN_ALPHA
#define MIN_ALPHA 0.1
#endif

const float PI = 3.141592653589793638;
#ifndef LIGHT_DIST_SCALE
#define LIGHT_DIST_SCALE 5
#endif
#ifdef IBL
const float MAX_REFLECTION_LOD = 4;
#endif

#ifdef SHADOWS
#ifndef SHADOW_SAMPLING_OFFSET
#define SHADOW_SAMPLING_OFFSET 4
#endif
#ifndef SHADOW_BIAS_MIN
#define SHADOW_BIAS_MIN 0.000005
#endif
#ifndef SHADOW_BIAS_MAX
#define SHADOW_BIAS_MAX 0.0005
#endif
#endif

#ifdef HDR
#ifndef HDR_TRSH
#define HDR_TRSH 0.7
#endif
#endif

#ifdef TONE_MAP
#ifndef EXPOSURE
#define EXPOSURE 1.0
#endif
#endif

#ifdef SSAO
#ifndef SSAO_KSIZE
SSAO kernel size required
#endif
#ifndef SSAO_RADIUS
SSAO radius required
#endif
#ifndef SSAO_BIAS
SSAO bias required
#endif
float ssao(vec3 pos)
{
  float occlusion = 0;
  for(int i=0; i<SSAO_KSIZE; i++)
  {
    vec3 sample = pos + samples[i] * SSAO_RADIUS;
    float sampleDepth = texture(posmap, sample.xy).z;
    float rangeCheck = smoothstep(0, 1, SSAO_RADIUS / abs(pos.z - sampleDepth));
    occlusion += sampleDepth >= sample.z+SSAO_BIAS ? rangeCheck : 0;
  }
  return 1 - occlusion/SSAO_KSIZE;
}
#endif

#ifdef SOBEL
#ifndef SOBEL_SAMPLES
#define SOBEL_SAMPLES 1
#endif
const mat3 sx = mat3(
    1.0, 2.0, 1.0,
    0.0, 0.0, 0.0,
    -1.0, -2.0, -1.0
    );

const mat3 sy = mat3(
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
    );

float sobel(sampler2D src, vec2 uv, bool absolute)
{
  vec2 texel_nsize = vec2(SOBEL_SAMPLES) / textureSize(src, 0); // get normalized size of a single texel

  mat3 I;
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
    {
      vec3 sample = texture(src, uv+vec2(i,j)*texel_nsize).xyz;
      if(!absolute) sample = sample/2.0+0.5;
      else sample = sample*2-1;
      I[i][j] = length(sample);
      //if(I[i][j] == 0) I[i][j]=length(vec3(1));
    }

  float gx = 0;
  float gy = 0;
  for (int i=0;i<3; i++)
  {
    gx += dot(sx[i], I[i]);
    gy += dot(sy[i], I[i]);
  }
  return length(vec2(gx,gy));
}
#endif


struct Light
{
#define LIGHT_DISABLED uint(0)
#define LIGHT_DIR uint(1)
#define LIGHT_POINT uint(2)
#define LIGHT_SPOT uint(3)
  // Since data in UBO can't be 3 aligned (even with std140),
  // vec4 are used in place
  vec4 pos;

  //vec4 color;
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  vec4 attenuation;

  vec4 dir;
  vec4 cutoff;

  uint type;
};

uniform Constants
{
  mat4 lspace;
  vec3 camera;
  int debug;
};

layout(std140) uniform Lights
{
  Light lights[LIGHTS_NUM];
};

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

in vec2 uv;

uniform sampler2D colmap;
uniform sampler2D posmap;
uniform sampler2D normmap;
uniform sampler2D mromap;
uniform sampler2D emmap;
#ifdef IBL
uniform samplerCube irrmap;
//uniform samplerCube envmap;
uniform sampler2D brdf_int;
#endif
#ifdef SHADOWS
uniform sampler2D shmap;
#endif

struct MaterialInfo
{
  float alphaRoughness;

  vec3 F0;
  vec3 F90;

  vec3 diffColor;
  vec3 specColor;
};
// Set in pbr()
MaterialInfo mInfo = MaterialInfo(0, vec3(0), vec3(0), vec3(0), vec3(0));

out vec4 main_out;
#ifdef HDR
out vec4 hdr;
#endif


#ifdef SHADOWS
float shadow(vec3 fragpos, float bias)
{
  vec4 pos_lspace4 = lspace*OEXT34(fragpos);
  vec3 pos_lspace = pos_lspace4.xyz / pos_lspace4.w;
  pos_lspace = pos_lspace * 0.5 + 0.5;
  if(pos_lspace.z > 1) return 1.0; // No shadow beyond far clipping pane

  vec2 texelSize = 1.0 / textureSize(shmap, 0);
  float shadow = 0.0;
  for(int x=-SHADOW_SAMPLING_OFFSET; x<=SHADOW_SAMPLING_OFFSET; x++)
    for(int y=-SHADOW_SAMPLING_OFFSET; y<=SHADOW_SAMPLING_OFFSET; y++)
    {
      float closestDepth = texture(shmap, pos_lspace.xy + vec2(x,y)*texelSize).r;
      shadow += pos_lspace.z-bias > closestDepth ? 0.0 : 1.0;
    }
  return shadow / pow(SHADOW_SAMPLING_OFFSET*2+1, 2);
}
#endif

float plight_int(Light light, vec3 fragpos)
{
  float light_dist = distance(light.pos.xyz, fragpos);
  light_dist *= LIGHT_DIST_SCALE;
  //float attenuation = light.attenuation.x + light_dist*light.attenuation.y + light_dist*light_dist*light.attenuation.z;
  vec3 light_dists = vec3(1, light_dist, light_dist*light_dist);
  float attenuation = dot(light_dists, light.attenuation.xyz);
  return 1.0/attenuation;
}

float slight_int(Light light, vec3 fragpos)
{
  vec3 light_dir = normalize(light.pos.xyz - fragpos);
  float theta = dot(light_dir, normalize(-light.dir.xyz));
  float epsilon = light.cutoff.x - light.cutoff.y;
  return clamp(((theta-light.cutoff.y)/epsilon), 0, 1) * plight_int(light, fragpos);
}

float light_int(Light light, vec3 fragpos)
{
  if(light.type == LIGHT_DISABLED) return 0.0;
  if(light.type == LIGHT_DIR) return 1.0;
  if(light.type == LIGHT_POINT) return plight_int(light, fragpos);
  if(light.type == LIGHT_SPOT) return slight_int(light, fragpos);
  return 100.0;
}


// Schlick's Fresnel
vec3 fresnelSchlick(vec3 H, vec3 V, vec3 F0, vec3 F90)
{
  float HdotV = max(dot(H,V), 0);

  return F0 + (F90-F0) * pow(1-HdotV, 5);
}

// Sebastien Lagarde's correction of Schlick's Fresnel
// to take roughness into account
vec3 fresnelSchlickRough(float NdotV, vec3 F0, float roughness)
{
  vec3 F90 = max(vec3(1-roughness), F0);
  return F0 + (F90-F0) * pow(1-NdotV, 5);
}

// Trowbridge-Reitz GGX normal distribution
// a2 / pi * ((n.h)2*()2 + 1)2
// mathematically identical to Khronos
float distribGGX(vec3 N, vec3 H)
{
  float a = mInfo.alphaRoughness;
  float a2 = mInfo.alphaRoughness * mInfo.alphaRoughness;

  float NdotH = max(dot(N, H), 0);
  float NdotH2 = NdotH*NdotH;

  float denom_part = NdotH2 * (a2-1) + 1;
  denom_part = max(denom_part, 0.001);

  return a2 / (PI * denom_part*denom_part);
}

// GGX (G')
// 2(n.x) / n.x + sqrt(a2+(1-a2)(n.x)2)
float geomGGX(float NdotX)
{
  float a = mInfo.alphaRoughness;
  return 2*NdotX / (NdotX + length(vec2(a, (1-a*a)*NdotX)));
}

// Schlick approximation of Beckmann's G'
// k = a*sqrt(2/pi)
// G'(x) = n.x / (n.x)(1-k)+k
// UE4's Schlick correction (Schlick did an error)
// k = a/2
float geomSchlickGGX(float NdotX)
{
  /*float r = roughness+1;
    float k = r*r / 8.0;*/
  float a = mInfo.alphaRoughness;
  float k = a / 2.0;

  return NdotX / (NdotX*(1-k) + k);
}

// Smith geometric shadowing, different modelisations for G'
// G(l,v,h) = G'(l)G'(v)
float geomSmith(float NdotV, float NdotL)
{
  return geomSchlickGGX(NdotV) * geomSchlickGGX(NdotL);
}

#ifdef KHRONOS
float GGXkhronos(float a2, float NdotX, float NdotY)
{
  return NdotX * sqrt(a2 + NdotY*NdotY*(1-a2));
}
// Smith Joint GGX
float geomSmithKhronos(float NdotV, float NdotL)
{
  float a2 = mInfo.alphaRoughness * mInfo.alphaRoughness;
  float x = GGXkhronos(a2, NdotL, NdotV);
  float y = GGXkhronos(a2, NdotV, NdotL);
	float ggx = x+y;
	if(ggx>0) return 0.5 / (x+y);
	else return 0.0;
}
#endif

vec3 brdf(vec3 F, vec3 H, vec3 N, float NdotL, float NdotV)
{
	// mathematically identical to Khronos
  float D = distribGGX(N, H);
#ifdef KHRONOS
  float G = geomSmithKhronos(NdotV, NdotL);
	return D*F*G;
#else
  float G = geomSmith(NdotV, NdotL);

  // Keep denominator non-null
  float denom = max(4 * NdotV * NdotL, 0.001);

  if(debug==1) return vec3(D);
  else if(debug==2) return F;
  else if(debug==3) return vec3(G);
  else if(debug==4) return vec3(1.0/denom);
	else if(debug==5) return vec3(G/denom);
	else return D*F*G / denom;

#endif
}

vec3 compute_ambient(vec3 albedo, vec3 F0, float NdotV, vec3 N, float metallic, float roughness)
{
  vec3 F = fresnelSchlickRough(NdotV, F0, roughness);
  vec3 kD = (1 - F) * (1 - metallic);

#ifdef IBL
  vec3 irradiance = texture(irrmap, N).rgb;
  //irradiance = pow(irradiance, vec3(2.2));

  /*vec3 R = reflect(-V, N);
    vec3 prefiltered = textureLod(envmap, R, roughness * MAX_REFLECTION_LOD).rgb;
    vec2 envBRDF = texture(brdf_int, vec2(NdotV, roughness)).rg;
    vec3 specular = prefiltered * (F*envBRDF.x + envBRDF.y);
    specular *= 0.3;*/
  vec3 specular = mix(F0, albedo, metallic);
#else
  vec3 irradiance = vec3(1);
  vec3 specular = mix(F0, albedo, metallic);
#endif
  vec3 diffuse = kD * irradiance * albedo;
  //vec3 ambient = diffuse + specular;
  vec3 ambient = albedo;
  return ambient * 0.05;
}

vec3 compute_light(Light light, vec3 N, vec3 V, float NdotV, vec3 F0, vec3 fragpos, bool cast_shadows, float metallic, vec3 albedo, float roughness)
{
    float light_int = light_int(light, fragpos);
    if(light_int == 0)
      return vec3(0);
    //if(i !=0) light_int *= 20*i;

    vec3 light_dir = normalize(light.pos.xyz - fragpos);

#ifdef SHADOWS
    //float bias = max(0.05 * (1.0-dot(N, light_dir)), 0.0005);
    if(/*albedo.a>=0.1 &&*/ cast_shadows) // Don't cast shadow on transparent objects for now
    {
      float bias = max(SHADOW_BIAS_MAX * (1.0-dot(N, light_dir)), SHADOW_BIAS_MIN);
      light_int *= shadow(fragpos, bias);
      if(light_int == 0)
        return vec3(0);
    }
#endif

    float NdotL = max(dot(N, light_dir), 0);
    if(NdotL == 0) return vec3(0);


    // half dir
    vec3 H = normalize(light_dir + V);
#ifdef KHRONOS
    vec3 F = fresnelSchlick(H, V, F0, mInfo.F90);
#else
    vec3 F = fresnelSchlick(H, V, F0, vec3(1));
    vec3 kD = (1 - F) * (1 - metallic);
    vec3 diffuse = kD * albedo;
#endif

    vec3 specular = brdf(F, H, N, NdotL, NdotV);
    if(debug != 0) return specular;

    vec3 radiance = light.diffuse.rgb * light_int;

#ifdef KHRONOS
    vec3 diffContrib = (1-F) * mInfo.diffColor/PI;
    vec3 specContrib = specular;
    vec3 shade = NdotL * (diffContrib+specContrib);
    return radiance * shade;
#else
    return (specular + diffuse/PI) * radiance * NdotL;
#endif
}

vec3 pbr(vec3 albedo)
{
  vec3 fragpos = texture(posmap, uv).xyz;
  vec3 N = texture(normmap, uv).xyz;
  vec3 mro = texture(mromap, uv).rgb;
  float ao = mro.r;
  float metallic = clamp(mro.b, 0, 1);
  float roughness = clamp(mro.g, 0, 1);
  vec3 emiss = texture(emmap, uv).rgb;

  vec3 V = normalize(camera - fragpos);
  float NdotV = max(dot(N, V), 0);
  //if(NdotV == 0) return vec3(0);
  // check if the previous line is actually true

  // F0 surface reflection color (and intensity) at zero incidence
  vec3 F0 = vec3(0.04);                // Non metallic materials look good with this
  vec3 specColor = mix(F0, albedo, metallic);    // Metallic ones are simply mapped between this and their albedo
#ifdef KHRONOS
  float reflectance = max(max(specColor.r, specColor.g), specColor.b);
  vec3 F90 = vec3(clamp(reflectance*50, 0, 1));
#else
  F0 = specColor;
  vec3 F90 = vec3(1);
#endif

  vec3 diffColor = albedo * (1-F0)*(1-metallic);
  mInfo = MaterialInfo(
      roughness*roughness,
      F0,
      F90,
      diffColor,
      specColor
  );

  vec3 color = emiss + ao * compute_ambient(albedo, F0, NdotV, N, metallic, roughness);
	if(debug==6) return color;
	else if(debug != 0) color = vec3(0);

  for(int i=0; i<lights.length(); i++)
  {
    Light light = lights[i];
    color += compute_light(lights[i], N, V, NdotV, F0, fragpos, i==0, metallic, albedo, roughness);
    // Ignore other lights for debugging phase
    break;
  }

  return color;
}

#ifdef TONE_MAP
vec3 expos_tone_map(vec3 color)
{
  // exposure=1 seems to be the neutral value
  return color * EXPOSURE;
  return vec3(1) - exp(-color*EXPOSURE);
}

vec3 reinhard_tone_map(vec3 color)
{
  return color / (color + vec3(1));
}
#endif

void main()
{
  vec4 albedo = texture(colmap, uv);
  if(albedo.a < MIN_ALPHA)
  {
    discard;
    main_out = vec4(0);
  }

  vec3 color = pbr(albedo.rgb);

#ifdef SOBEL
  float sobel = sobel(normmap, uv, false);
  //sobel = smoothstep(0.4,0.6,sobel);
  if(sobel>0.2)
  {
    main_out = vec4(0);
    return;
  }
  //else color = vec3(1);
#endif

#ifdef HDR
  if(dot(color, vec3(0.2126,0.7152,0.0722)) > HDR_TRSH)
    hdr = vec4(color, 1);
  //	else hdr = texture(emmap, uv);
  else hdr = vec4(0);
#endif

  if(debug == 0)
  {
#ifdef TONE_MAP
    /*vec4 acolor = texture(alpha, uv);
      color = mix(color, acolor.rgb, acolor.a) + texture(hdr, uv).rgb;*/
    color = TONE_MAP(color);
#endif

#ifdef GAMMA
      color = pow(color, vec3(1.0/GAMMA_CORR));
#endif
  }

  main_out = vec4(color, albedo.a);
}
