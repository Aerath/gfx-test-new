#version 330

in vec3 pos;
in vec3 norm;
in vec2 tex;

uniform Transform
{
    mat4 vp;
    mat4 model;
};

#ifndef NO_OUT
out vec3 wpos;
out vec3 normal;
out vec2 uv;
#endif

void main()
{
#ifndef NO_OUT
    normal = normalize((transpose(inverse(model)) * vec4(norm,1)).xyz);

    uv = tex;

    wpos = (model * vec4(pos,1)).xyz;
#endif

    gl_Position = vp * model * vec4(pos,1);
}
