#version 330

#ifndef MIN_ALPHA
#define MIN_ALPHA 0.1
#endif

in vec3 wpos;
in vec3 normal;
in vec2 uv;

uniform sampler2D coltex;
uniform sampler2D normtex;
uniform sampler2D mrtex;
uniform sampler2D aotex;
uniform sampler2D emtex;

uniform NTextured
{
	bool ntextured;
};

uniform PbrMaterial
{
    vec4 albedoFact;
    float roughnessFact;
    float metallnessFact;
    //vec4 emissiveFact;
    float occlusionFact;
    uint alpha_mode;
};

out vec4 colmap;
out vec3 posmap;
out vec3 normmap;
out vec3 mromap;
out vec3 emmap;

mat3 cotangent_frame(vec3 wpos, vec3 normal, vec2 uv)
{
    vec3 dp1perp = cross(normal, dFdx(wpos));
    vec3 dp2perp = cross(dFdy(wpos), normal);

    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

void main()
{
    vec4 col = texture(coltex, uv) * albedoFact;
    //vec4 col = pow(texture(coltex, uv),vec4(vec3(2.2),1)) * albedoFact;
    if(col.a < MIN_ALPHA)
    {
        discard;
        return;
    }
    colmap = col;

    posmap = wpos;

	// Probably due to alignment or type length issues
	// (NTextured using 16 bytes rather than 1), maybe causing this "bool"
	// to be 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 rather than 01
	if(ntextured)
	{
		mat3 tbn = cotangent_frame(wpos, normal, uv);
		vec3 norm = texture(normtex, uv).xyz;
		norm = normalize(norm * 2 - 1);
		normmap = normalize(tbn*norm);
	}
	else normmap = normal;

    vec2 mr = texture(mrtex, uv).gb;
    float ao = texture(aotex, uv).r;
    mromap = vec3(ao, mr) * vec3(occlusionFact, metallnessFact, roughnessFact);
    //FIXME: may need clamping

    emmap = texture(emtex, uv).rgb /* emissiveFact*/;
}
