#version 330

in vec3 uv;

uniform samplerCube cubemap;

out vec4 main_col;

void main()
{
	vec3 color = texture(cubemap, uv).rgb;
	color = pow(color, vec3(2.2));
	main_col = vec4(color,1);
}
