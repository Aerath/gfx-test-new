#version 330 core

in vec2 uv;

uniform sampler2D src;
const float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);
//uniform float weight[5] = float[] (0.382928, 0.241732, 0.060598, 0.005977, 0.000229); // From dev.theomader.com/gaussian-kernel-calculator

out vec4 dst;

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif


/* Currently, in CPU code, ping-pong buffers are setup
 * as hdr ->(H) gaussbuf ->(V) hdr, gaussbuf being half
 * resolution of hdr (w/2, h/2).
 * For some reason, this difference in size causes a
 * non-uniform blur, with height larger than width.
 * To overcome this, as the actual reason is not
 * understood, PIX_STEP is defined differently
 * accoding to the direction of blur.
 */

#ifdef HORIZONTAL

#ifdef VERTICAL
Cant set both VERTICAL and HORIZONTAL
#else
#define DIR vec2(1,0)
#define PIX_STEP 2
#endif

#else

#ifdef VERTICAL
#define DIR vec2(0,1)
#define PIX_STEP 1
#else
//#define DIR vec2(1,0)
Either VERTICAL or HORIZONTAL needs to be defined
#endif

#endif

void main()
{
  vec2 dir = DIR / textureSize(src, 0); // get size of single texel

  vec3 res = texture(src, uv).rgb * weight[0];
  for(int i=1; i<5; i++)
  {
    float f = PIX_STEP * float(i);
    res += texture(src, uv + f*dir).rgb * weight[i];
    res += texture(src, uv - f*dir).rgb * weight[i];
  }

  dst = OEXT34(res);
}
