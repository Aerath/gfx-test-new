#version 330

in vec2 pos;
out vec2 uv;

void main()
{
    uv = 0.5*(pos + vec2(1,1));
    gl_Position = vec4(pos,0,1);
}
