#version 330

#ifdef TONE_MAP
#ifndef EXPOSURE
#define EXPOSURE 1.0
#endif
#endif

in vec2 uv;

uniform sampler2D main_col;
uniform sampler2D hdr;

out vec3 screen;

#ifdef GAMMA_CORR
vec3 gamma_corr(vec3 color, float gamma) // tbd after tone mapping
{
	return pow(color, vec3(1.0/gamma));
}
#endif

#ifdef TONE_MAP
vec3 expos_tone_map(vec3 color, float exposure)
{
	// expos=1 seems to be the neutral value
  return color * exposure;
	return vec3(1) - exp(-color*exposure);
}

vec3 reinhard_tone_map(vec3 color)
{
	return color / (color + vec3(1));
}
#endif

void main()
{
	vec3 color = texture(main_col, uv).rgb;
	//vec4 acolor = texture(alpha, uv);
	color = /*mix(*/color/*, acolor.rgb, acolor.a)*/ + texture(hdr, uv).rgb;
#ifdef TONE_MAP
	color = TONE_MAP(color, EXPOSURE);
#endif
#ifdef GAMMA_CORR
	color = gamma_corr(color, GAMMA_CORR);
#endif
	screen = color;
}
