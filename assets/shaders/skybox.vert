#version 330 core

in vec3 pos;

uniform ViewProj
{
    mat4 proj;
    mat4 view;
};

out vec3 uv;

void main()
{
	uv = pos;

	vec4 pos2 = proj*mat4(mat3(view)) * vec4(pos, 1);

	gl_Position = pos2.xyww;
}
