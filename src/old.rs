#[macro_use]
extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;
extern crate cgmath;
extern crate image;

use gfx::traits::FactoryExt;
use gfx::Device;
use gfx_window_glutin as gfx_glutin;
use glutin::{GlContext, GlRequest};
use glutin::Api::OpenGl;
use cgmath::prelude::*;
use cgmath::{Vector3,vec3,Matrix4,Rad};

pub type ColorFormat = gfx::format::Rgba8;
pub type DepthFormat = gfx::format::DepthStencil;

gfx_defines!
{
    vertex Vertex
    {
        pos: [f32;3] = "a_Pos",
        uv: [f32;2] = "a_UV",
        color: [f32;4] = "a_Color",
    }

    pipeline pipe
    {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        model: gfx::Global<[[f32;4];4]> = "model",
        view: gfx::Global<[[f32;4];4]> = "view",
        proj: gfx::Global<[[f32;4];4]> = "proj",
        textured: gfx::Global<i32> = "textured",
        sampler: gfx::TextureSampler<[f32;4]> = "t_sampler",
        //out: gfx::RenderTarget<ColorFormat> = "Target0",
        out: gfx::BlendTarget<ColorFormat> = ("Target0", gfx::state::ColorMask::all(), gfx::preset::blend::ALPHA),
        out_depth: gfx::DepthTarget<DepthFormat> = gfx::preset::depth::LESS_EQUAL_WRITE,
    }
}

const RED: [f32;4] = [1.0,0.0,0.0,1.0];
const GREEN: [f32;4] = [0.0,1.0,0.0,1.0];
const BLUE: [f32;4] = [0.0,0.0,1.0,1.0];
const WHITE: [f32;4] = [1.0;4];
const BLACK: [f32;4] = [0.0,0.0,0.0,1.0];

#[derive(Debug)]
struct Square<R: gfx::Resources>
{
    pub pos: Vector3<f32>,
    pub width: f32,
    pub height: f32,
    pub vbuf: Option<gfx::handle::Buffer<R, Vertex>>,
    pub slice: Option< gfx::Slice<R>>
}

impl<R: gfx::Resources> Default for Square<R>
{
    fn default() -> Self
    {Self{pos:vec3(0.0,0.0,0.0), width:1.0, height:1.0, vbuf:None, slice:None}}
}

impl<R: gfx::Resources> Square<R>
{
    #[inline]
    fn gen_vertices(w:f32, h:f32) -> [Vertex;4]
    {
        //let w = std::ops::Drop(Buffer
        let w = w/2.0;
        let h = h/2.0;
        [
            Vertex {pos:[-w,h,0.0], color:RED, uv:[0.0;2]},
            Vertex {pos:[w,h,0.0], color:GREEN, uv:[1.0,0.0]},
            Vertex {pos:[w,-h,0.0], color:BLUE, uv:[1.0;2]},
            Vertex {pos:[-w,-h,0.0], color:WHITE, uv:[0.0,1.0]}
        ]
    }

    #[inline]
    fn indices<'a>() -> &'a[u16]
    {&[0,1,2,2,3,0]}

    fn new(w:f32, h:f32) -> Self
    {
        Self
        {
            width: w,
            height: h,
            ..Default::default()
        }
    }

    fn upload<F: gfx::traits::FactoryExt<R>>(mut self, fact: &mut F) -> Self
    {
        let (vbuf, slice) = fact.create_vertex_buffer_with_slice(&Self::gen_vertices(self.width, self.height), Self::indices());
        self.vbuf = Some(vbuf);
        self.slice = Some(slice);
        self
    }
}

fn load_texture<F,R>(fact: &mut F, path: &str) -> gfx::handle::ShaderResourceView<R, [f32;4]>
    where F: gfx::Factory<R>, R: gfx::Resources
{
    let img = image::open(path).unwrap().to_rgba();
    let (w,h) = img.dimensions();
    let kind = gfx::texture::Kind::D2(w as u16, h as u16, gfx::texture::AaMode::Single);
    let (_, view) = fact.create_texture_immutable_u8::<ColorFormat>(kind, gfx::texture::Mipmap::Provided, &[&img]).unwrap();
    view
}

fn main()
{
    const WIDTH:u32 = 512;
    const HEIGHT:u32 = 512;

    let win_builder = glutin::WindowBuilder::new()
        .with_title("gfx-rs test")
        .with_dimensions((512, 512).into());
    let cont_builder = glutin::ContextBuilder::new()
        .with_gl(GlRequest::Specific(OpenGl,(3,3)))
        .with_gl_profile(glutin::GlProfile::Core)
        .with_vsync(true)
        .with_multisampling(0);
    let mut events_loop = glutin::EventsLoop::new();
    let (win, mut dev, mut fact, col_view, depth_view) =
        gfx_glutin::init::<ColorFormat, DepthFormat>(win_builder, cont_builder, &events_loop);

    let pso = fact.create_pipeline_simple(include_bytes!("../assets/shader/simple.vert"), include_bytes!("../assets/shader/simple.frag"), pipe::new()).unwrap();

    let mut encoder: gfx::Encoder<_,_> = fact.create_command_buffer().into();
    let texture = load_texture(&mut fact, "assets/img/square.png");
    let square = Square::new(0.7,0.7).upload(&mut fact);
    let slice = square.slice.unwrap();
    let mut data = pipe::Data
    {
        vbuf: square.vbuf.unwrap(),
        //mvp: fact.create_constant_buffer(1),
        model: Matrix4::identity().into(),
        view: Matrix4::identity().into(),
        proj: Matrix4::identity().into(),
        textured: 1,
        sampler: (texture, fact.create_sampler_linear()),
        out: col_view,
        out_depth: depth_view
    };

    let mut running = true;
    let mut prev_frame = std::time::Instant::now();
    let mut pos: f32 = 0.0;
    while running
    {
        events_loop.poll_events(|ev|
        {
            use glutin::WindowEvent::*;
            if let glutin::Event::WindowEvent{event, ..} = ev
            {
                match event
                {
                    CloseRequested | KeyboardInput{input: glutin::KeyboardInput{virtual_keycode:  Some(glutin::VirtualKeyCode::Escape), ..}, ..} => running = false,
                    Resized(_) => gfx_window_glutin::update_views(&win, &mut data.out, &mut data.out_depth),
                    _ => {}
                }
            }
        });

        let elapsed = prev_frame.elapsed();
        prev_frame = std::time::Instant::now();
        let elapsed = elapsed.as_secs() as f32 * 1_000.0 + elapsed.subsec_nanos() as f32 / 1_000_000.0;
        pos += elapsed / 7500.0;
        if pos > 1.5 {pos %= 1.5;}

        encoder.clear(&data.out, BLACK);
        encoder.clear_depth(&data.out_depth, 1.0);
        //encoder.update_buffer(&data.mvp, &[mvp], 0).unwrap();
        data.model = Matrix4::from_translation(Vector3::unit_x() * pos).into();
        encoder.draw(&slice, &pso, &data);

        data.model = (Matrix4::from_translation(vec3(-0.3, 0.5,0.0))*Matrix4::from_angle_y(Rad(pos*8.0))*Matrix4::from_scale(0.3)).into();
        encoder.draw(&slice, &pso, &data);

        encoder.flush(&mut dev);
        win.swap_buffers().unwrap();
        dev.cleanup();
    }
}
