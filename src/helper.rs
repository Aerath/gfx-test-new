use cgmath::{Matrix,Matrix4,SquareMatrix,Vector4};
use gfx;
use gfx::{traits::FactoryExt,gfx_pipeline_inner,gfx_defines,gfx_pipeline,gfx_constant_struct,gfx_constant_struct_meta,gfx_impl_struct_meta,gfx_vertex_struct,gfx_vertex_struct_meta};
use gfx::format::{Float,R16_G16_B16,R16_G16,Unorm};
type Rgb16F = (R16_G16_B16, Float);

#[derive(Clone,Debug)]
pub(crate) struct PipelineObj<R,D>
	where R: gfx::Resources, D: gfx::pso::PipelineData<R>
{
	pub state: gfx::pso::PipelineState<R, D::Meta>,
	pub data: D
}

gfx_vertex_struct!(Vertex2
{
	pos: [f32;2] = "pos",
});

gfx_vertex_struct!(Vertex3NT
{
	pos: [f32;3] = "pos",
	norm: [f32;3] = "norm",
	tex: [f32;2] = "tex",
});

gfx_constant_struct!
{
	Light
	{
		pos: [f32;4] = "pos",
		ambient: [f32;4] = "ambient",
		diffuse: [f32;4] = "diffuse",
		specular: [f32;4] = "specular",
		attenuation: [f32;4] = "attenuation",

		direction: [f32;4] = "dir",
		cutoff: [f32;4] = "cutoff",
		ltype: LightType = "type",
		padding: [f32;3] = "padding",
	}
}

gfx_defines!
{
	constant ViewProj
	{
		proj: [[f32;4];4] = "proj",
		view: [[f32;4];4] = "view",
	}
	pipeline brdf_int
	{
		vbuf: gfx::VertexBuffer<Vertex2> = (),
		output: gfx::RenderTarget<(R16_G16,Unorm)> = "output",
	}
}

gfx_defines!
{
	pipeline irrmap_convol
	{
		vbuf: gfx::VertexBuffer<Vertex3NT> = (),
		cubemap: gfx::TextureSampler<[f32;4]> = "cubemap",
		view_proj: gfx::ConstantBuffer<ViewProj> = "ViewProj",
		output: gfx::RenderTarget<Rgb16F> = "output",
	}
}

#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u32)]
pub enum LightType
{
	Disabled,
	Directional,
	Point,
	Spot
}

impl gfx::shade::core::Formatted for LightType
{
	fn get_format() -> (gfx::shade::core::BaseType, gfx::shade::core::ContainerType)
	{
		use gfx::shade::core::{BaseType::*,ContainerType::*};
		(U32,Single)
	}
}

impl Default for Light
{
	fn default() -> Self
	{
		Self
		{
			ltype: LightType::Disabled,
			pos: [2.0, 0.8, 2.0, 0.0],
			ambient: [1.0, 1.0, 1.0, 0.0],
			diffuse: [1.0, 1.0, 1.0, 0.0],
			specular: [1.0, 1.0, 1.0, 0.0],
			attenuation: [1.0, 0.0, 0.1, 0.0],
			direction: [1.0, -0.5, 0.0, 0.0],
			cutoff: [0.91, 0.82, 0.0, 0.0],
			padding: [0.0,0.0,0.0],
		}
	}
}

pub const SQUARE: [Vertex2;4] =
[
	Vertex2{pos:[-1.0, -1.0]},
	Vertex2{pos:[-1.0, 1.0]},
	Vertex2{pos:[1.0, -1.0]},
	Vertex2{pos:[1.0, 1.0]},
];

pub const CUBE_V: [Vertex3NT; 8] =
[
	Vertex3NT
	{
		pos: [-1.0, 1.0, 1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [0.0, 1.0],
	},
	Vertex3NT
	{
		pos: [1.0, 1.0, 1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [1.0, 1.0],
	}, // 1
	Vertex3NT
	{
		pos: [1.0, -1.0, 1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [1.0, 0.0],
	}, // 2
	Vertex3NT
	{
		pos: [-1.0, -1.0, 1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [0.0, 0.0],
	}, // 3
	Vertex3NT
	{
		pos: [-1.0, 1.0, -1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [0.0, 1.0],
	}, // 4
	Vertex3NT
	{
		pos: [1.0, 1.0, -1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [1.0, 1.0],
	}, // 5
	Vertex3NT
	{
		pos: [1.0, -1.0, -1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [1.0, 0.0],
	}, // 6
	Vertex3NT
	{
		pos: [-1.0, -1.0, -1.0],
		norm: [0.0, 0.0, 1.0],
		tex: [0.0, 0.0],
	}, // 7
];

pub const CUBE_I: [u16; 36] =
[
	3, 1, 0, 3, 2, 1,
	2, 5, 1, 2, 6, 5,
	6, 4, 5, 6, 7, 4,
	7, 0, 4, 7, 3, 0,
	0, 5, 4, 0, 1, 5,
	7, 2, 3, 7, 6, 2,
];

fn load_img(filepath: impl AsRef<str>) -> image::ImageBuffer<image::Rgba<u8>, Vec<u8>>
{
	let filepath = filepath.as_ref();
	assert!(filepath.len() > 4);

	/*let format = match &*filepath[filepath.len()-3..].to_string().to_uppercase()
	{
		"PNG" => image::ImageFormat::Png,
		"JPG" | "PEG" => image::ImageFormat::Jpeg, // TODO: Actually get extension
		"GIF" => image::ImageFormat::Gif,
		"BMP" => image::ImageFormat::Bmp,
		"TGA" => image::ImageFormat::Tga,
		_ => panic!("Unsupported image format for {}", filepath)
	};

	let mut img = image::load(
		BufReader::new(File::open(filepath).expect(&format!("Failed to open {}", filepath)[..])),
		format,
	).unwrap();*/

    let mut img = image::open(filepath).unwrap();
	//if format != image::ImageFormat::Tga
	if &*filepath[filepath.len()-3..].to_string().to_uppercase() != "TGA"
	{ img = img.flipv() }
	img.to_rgba()
}

pub fn load_tex<X,F,R,S>(fact: &mut F, filepath: S) -> (gfx::handle::Texture<R, <X as gfx::format::Formatted>::Surface>, gfx::handle::ShaderResourceView<R, <X as gfx::format::Formatted>::View>)
	where R: gfx::Resources, F: gfx::traits::FactoryExt<R>, X: gfx::format::TextureFormat, S: AsRef<str>
{
	let img = load_img(filepath);
	let dims = img.dimensions();
	let kind = gfx::texture::Kind::D2(dims.0 as u16, dims.1 as u16, gfx::texture::AaMode::Single);
	fact.create_texture_immutable_u8::<X>(kind, gfx::texture::Mipmap::Provided, &[&img]).unwrap()
}

pub fn load_cubemap<X,I,R,F,S>(fact: &mut F, filepathes: I) -> (gfx::handle::Texture<R, <X as gfx::format::Formatted>::Surface>, gfx::handle::ShaderResourceView<R, <X as gfx::format::Formatted>::View>)
	where I: IntoIterator<Item=S>, S: AsRef<str>, R: gfx::Resources, F: gfx::traits::FactoryExt<R>, X: gfx::format::TextureFormat
{
	let imgs: Vec<_> = filepathes.into_iter().map(load_img).collect();
	let data: [&[u8];6] = [&imgs[0], &imgs[1], &imgs[2], &imgs[3], &imgs[4], &imgs[5]];
	let kind = gfx::texture::Kind::Cube(imgs[0].dimensions().0 as u16);

	fact.create_texture_immutable_u8::<X>(kind, gfx::texture::Mipmap::Provided, &data).unwrap()
}

macro_rules! load_shad
{
	($path:expr) =>
	{
		&load_shader($path, vec!())[..]
	};
	($path:expr, $($def:expr),+) =>
	{
		&load_shader($path, vec!($($def),+))[..]
	}
}

pub fn load_shader<'a>(filepath: impl AsRef<std::path::Path>, defines: Vec<&'a str>) -> Vec<u8>
{
	let filepath = filepath.as_ref();
	let mut content = std::fs::read_to_string(filepath)
		.expect(&format!("Failed to read {}", filepath.display())[..]);
	for def in defines
	{
		let def = format!("\n#define {}\n", def);
		content = content.replacen("\n", &def[..], 1);
	}
	content.into_bytes()
}

/*/// `encoder.flush` must be called to actually compute the irradiance map
//pub fn convolute_irrmap<X,F,R,C>(fact: &mut F, encoder: &mut gfx::Encoder<R,C>, skybox: &gfx::handle::ShaderResourceView<R, <X as gfx::format::Formatted>::View>, size: u16)
//	-> <X as gfx::format::Formatted>::View
pub fn convolute_irrmap<F,R,C>(fact: &mut F, encoder: &mut gfx::Encoder<R,C>, skybox: &gfx::handle::ShaderResourceView<R, [f32;4]>, size: u16)
	-> gfx::handle::ShaderResourceView<R,[f32;3]>
	where R: gfx::Resources, F: gfx::traits::Factory<R>, C: gfx::CommandBuffer<R>
{
	let sampler = fact.create_sampler_linear();
	let (vbuf,slice) = fact.create_vertex_buffer_with_slice(&CUBE_V, &CUBE_I[..]);
	let ppline_state = fact.create_pipeline_simple(load_shad!("assets/shaders/skybox.vert"), load_shad!("assets/shaders/irrmap.frag"), irrmap_convol::new())
		.expect("Failed to link irradiance map convolution program");
	let ppline_data = irrmap_convol::Data
	{
		vbuf,
		view_proj: fact.create_constant_buffer(1),
		cubemap: (skybox.clone(), sampler),
		output: fact.create_render_target(1,1).unwrap().2 // fake render target
	};

	let proj: [[f32; 4]; 4] = cgmath::perspective(Deg(90f32), 1f32, 0.1f32, 10f32).into();
	let origin = Point3::new(0.0, 0.0, 0f32);
	/*let views: [[[f32; 4]; 4]; 6] = [
		Matrix4::look_at(origin, Point3::new(1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
		Matrix4::look_at(origin, Point3::new(-1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
		Matrix4::look_at(origin, Point3::new(0.0, 1.0, 0.0), Vector3::unit_z()).into(),
		Matrix4::look_at(origin, Point3::new(0.0, -1.0, 0.0), -Vector3::unit_z()).into(),
		Matrix4::look_at(origin, Point3::new(0.0, 0.0, 1.0), -Vector3::unit_y()).into(),
		Matrix4::look_at(origin, Point3::new(0.0, 0.0, -1.0), -Vector3::unit_y()).into(),
	];*/

	//let mut view_proj = ViewProj{proj, view:views[0]};
	let mut view_proj = ViewProj{proj, view: Matrix4::from_scale(0.0).into()};
	let faces: Vec<gfx::handle::Texture<_,_>> = gfx::texture::CUBE_FACES.iter().map(|f|
	{
		view_proj.view = match f
		{
			PosX => Matrix4::look_at(origin, Point3::new(1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
			NegX => Matrix4::look_at(origin, Point3::new(-1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
			PosY => Matrix4::look_at(origin, Point3::new(0.0, 1.0, 0.0), Vector3::unit_z()).into(),
			NegY => Matrix4::look_at(origin, Point3::new(0.0, -1.0, 0.0), -Vector3::unit_z()).into(),
			PosZ => Matrix4::look_at(origin, Point3::new(0.0, 0.0, 1.0), -Vector3::unit_y()).into(),
			NegZ => Matrix4::look_at(origin, Point3::new(0.0, 0.0, -1.0), -Vector3::unit_y()).into(),
		};
		encoder.update_buffer(&ppline_data.view_proj, &[view_proj], 0).unwrap();
		let output = fact.create_render_target(size,size).unwrap();
		ppline_data.output = output.2;
		encoder.draw(&slice, &ppline_state, &ppline_data);

		output.0
	}).collect();
	let kind = gfx::texture::Kind::Cube(size);
	let data: [&[gfx::handle::Texture<_,Rgb16F>]] = [faces[0], faces[1], &faces[2], &faces[3], &faces[4], &faces[5]];
	fact.create_texture_immutable::<Rgb16F>(kind, gfx::texture::Mipmap::Provided, &data).unwrap().1
}*/

/// `encoder.flush` must be called to actually compute the integration map
pub fn integrate_brdf<F,R,C>(fact: &mut F, encoder: &mut gfx::Encoder<R,C>, size: u16) -> gfx::handle::ShaderResourceView<R,[f32;2]>
	where R: gfx::Resources, F: gfx::Factory<R>, C: gfx::CommandBuffer<R>
{
	let output = fact.create_render_target(size, size).unwrap();
	let (vbuf,slice) = fact.create_vertex_buffer_with_slice(&SQUARE, ());
	let ppline_state = fact.create_pipeline_simple(load_shad!("assets/shaders/minim.vert"), load_shad!("assets/shaders/brdf_int.frag"), brdf_int::new())
		.expect("Failed to link brdf integration program");
	let ppline_data = brdf_int::Data
	{
		vbuf,
		output: output.2
	};
	encoder.draw(&slice, &ppline_state, &ppline_data);
	output.1
}

impl std::ops::Mul<Vertex3NT> for Matrix4<f32>
{
	type Output = Vertex3NT;

	fn mul(self, vertex: Vertex3NT) -> Vertex3NT
	{
		let inverse = self.invert();
		Vertex3NT
		{
			pos: (self * Vector4::new(vertex.pos[0], vertex.pos[1], vertex.pos[2], 1.0))
				.truncate()
				.into(),
			//norm: self.invert().unwrap().transpose().transform_vector(vertex.norm.into()).into(),
			//TODO: Find why transform matrix can be non-invertible (endless_floor triggers a crash
			//because of that)
			norm:
				(if let Some(inverse)=inverse{inverse.transpose()}else{Self::identity()}				  // Possibly give up on
				   * Vector4::new(vertex.norm[0], vertex.norm[1], vertex.norm[2], 1.0))
				.truncate()
				.into(), // normals transform
			..vertex
		}
	}
}
