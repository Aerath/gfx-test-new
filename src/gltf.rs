use cgmath::Matrix4;
use std::path::Path;
use gfx::{gfx_constant_struct,gfx_constant_struct_meta,gfx_impl_struct_meta};
use gfx::handle::ShaderResourceView;
use gfx::format::{Rgba8,Srgba8};

use crate::helper::{load_tex,Vertex3NT,PipelineObj};

const BLACK: usize = usize::max_value();
const WHITE: usize = usize::max_value()-1;

gfx_constant_struct!
{
	PbrMaterial
	{
		albedo_factor: [f32; 4] = "albedoFact",
		roughness_factor: f32 = "roughnessFact",
		metallic_factor: f32 = "metallnessFact",
		//emissive_factor: [f32; 4] = "emissiveFact",
		occlusion_factor: f32 = "occlusionFact",
		alpha_mode: AlphaMode = "alpha_mode",
	}
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum AlphaMode
{
	Opaque,
	Mask,
	Blend,
}

impl gfx::shade::core::Formatted for AlphaMode
{
	fn get_format() -> (gfx::shade::core::BaseType,gfx::shade::core::ContainerType)
	{
		use gfx::shade::core::{BaseType,ContainerType};
		(BaseType::U32,ContainerType::Single)
	}
}

impl Default for PbrMaterial
{
	fn default() -> Self
	{
		Self
		{
			albedo_factor: [1.0, 1.0, 1.0, 1.0],
			roughness_factor: 1.0,
			metallic_factor: 1.0,
			//emissive_factor: [1.0, 1.0, 1.0, 1.0],
			occlusion_factor: 0.0,
			alpha_mode: AlphaMode::Opaque,
		}
	}
}

pub(crate) enum Ppline<'a, R: gfx::Resources>
{
	Def1(&'a mut PipelineObj<R,crate::deffered1::Data<R>>),
	Shad(&'a mut PipelineObj<R,crate::shadow::Data<R>>),
}

#[derive(Clone,Debug)]
struct Node<R: gfx::Resources>
{
	pub children: Vec<Node<R>>,
	slice: gfx::Slice<R>,
	material: usize,
	texture: usize,
	nmap: Option<usize>,
	mrmap: usize,
	aomap: usize,
	emissmap: usize,
}

impl<R: gfx::Resources> Node<R>
{
	fn draw(
		&self,
		encoder: &mut gfx::Encoder<R,impl gfx::CommandBuffer<R>>,
		ppline: &mut Ppline<R>,
		//uniforms: &U,
		//params: &DrawParameters,
		parent: &NodeHierarchy<R>,
	)
	{
		for child in &self.children
		{ child.draw(encoder, ppline, parent) }

		if self.slice.end != 0
		{
			use self::Ppline::*;
			match ppline
			{
				Shad(ppline) => encoder.draw(&self.slice, &ppline.state, &ppline.data),
				Def1(ppline) =>
				{
					//FIXME: check that index is not MAX_VALUE
					let material = parent.materials[self.material];
					//if parent.loaded_material != self.material
					{
						encoder.update_buffer(&ppline.data.material, &[material], 0).unwrap();
						//parent.loaded_material = self.material;
					}
					//if alphapass == match material.alpha_mode{AlphaMode::Blend=>true,_=>false}
					/*if match material.alpha_mode
					{
						AlphaMode::Blend => true,
						_ => false,
					} || !alphapass*/
					{
						/*let uniforms = puniform!(
							material,
							&puniform!(
								uniforms,
								&uniform!(textured: self.texture!=0, ntextured: self.nmap!=0, mrtextured: self.mrmap!=0, aotextured: self.aomap!=0, emtextured: self.emissmap!=0,
							albedotex:tex, nmap:nmap, mrmap:mrmap, aomap:aomap, emmap:emmap,alphapass:alphapass)
							)
						);*/
						ppline.data.coltex.0 = parent.textures[self.texture].1.clone();
						ppline.data.mrtex.0 = parent.textures[self.mrmap].1.clone();
						match self.nmap
						{
							None => encoder.update_buffer(&ppline.data.ntextured, &[false.into()], 0).unwrap(),
							Some(nmap) =>
							{
								encoder.update_buffer(&ppline.data.ntextured, &[true.into()], 0).unwrap();
								ppline.data.normtex.0 = parent.textures[nmap].1.clone()
							}
						}
						ppline.data.aotex.0 = parent.textures[self.aomap].1.clone();
						ppline.data.emtex.0 = parent.textures[self.emissmap].1.clone();
						encoder.draw(&self.slice, &ppline.state, &ppline.data)
					}
				}
			}
		}
	}

	fn update_idx_buffer(&mut self, buffer: &gfx::IndexBuffer<R>)
	{
		if self.slice.end != 0
		{self.slice.buffer = buffer.clone()}

		for n in &mut self.children
		{n.update_idx_buffer(buffer)}
	}

	fn update_tex_indices(&mut self, parent: &NodeHierarchy<R>)
	{
		find_tex_idx(&parent.textures, &mut self.texture);

		if let Some(index) = self.nmap
		{
			if let Some((i,_)) = parent.textures.iter().enumerate().find(|(_,(idx,_))| *idx==index)
			{ self.nmap = Some(i) }
		}

		find_tex_idx(&parent.textures, &mut self.mrmap);
		find_tex_idx(&parent.textures, &mut self.aomap);
		find_tex_idx(&parent.textures, &mut self.emissmap);

		for n in &mut self.children
		{n.update_tex_indices(parent)}
	}
}

#[derive(Clone,Debug)]
pub(crate) struct NodeHierarchy<R: gfx::Resources>
{
	nodes: Vec<Node<R>>,
	vbuf: gfx::handle::Buffer<R,Vertex3NT>,
	ibuf: gfx::IndexBuffer<R>,
	textures: Vec<(usize,ShaderResourceView<R,[f32;4]>)>,
	materials: Vec<PbrMaterial>,
	loaded_texture: usize,
	loaded_material: usize,
}

impl<R: gfx::Resources> NodeHierarchy<R>
{
	pub(crate) fn draw(
		&self,
		encoder: &mut gfx::Encoder<R,impl gfx::CommandBuffer<R>>,
		mut ppline: Ppline<R>,
	)
	{
		use self::Ppline::*;
		match &mut ppline
		{
			Shad(ppline) => ppline.data.vbuf = self.vbuf.clone(),
			Def1(ppline) => ppline.data.vbuf = self.vbuf.clone()
		}
		for node in &self.nodes
		{ node.draw(encoder, &mut ppline, self) }
	}
}

fn load_gltf_texture<'a, X, F, R, T>(
	fact: &mut F,
	dir: &Path,
	tex: Option<T>,
	store: &mut Vec<(usize,ShaderResourceView<R, <X as gfx::format::Formatted>::View>)>,
) where R: gfx::Resources, T: AsRef<gltf::Texture<'a>>, X: gfx::format::TextureFormat, F: gfx::traits::FactoryExt<R>
{
	if tex.is_none() { return }
	let tex = tex.unwrap();
	let tex = tex.as_ref();
	let index = tex.index();
	if store.iter().any(|(i,_)|*i==index)
	{ return }  // Texture already stored

	if let gltf::image::Source::Uri { uri, .. } = tex.source().source()
	{
		let path = dir.join(Path::new(uri));
		let path = path.to_str().unwrap();
		/*let img = load_img(path);
		let dims = img.dimensions();
		let kind = gfx::texture::Kind::D2(dims.0 as u16, dims.1 as u16, gfx::texture::AaMode::Single);
		//let texref = fact.create_texture_immutable_u8::<X>(kind, gfx::texture::Mipmap::Provided, &[&img]).unwrap();
		let texref = fact.create_texture_immutable_u8::<X>(kind, gfx::texture::Mipmap::Provided, &[&img]).unwrap();*/
		let texref = load_tex::<X,_,_,_>(fact, path);
		store.push((tex.index(), texref.1));
	}
	else { panic!("Unhandled image source type for {:#?}", tex) }
}

/**
 * Replace a gltf texture index with it's `store` array index
 */
fn find_tex_idx<R,T>(store: &[(usize,ShaderResourceView<R,T>)], index: &mut usize)
	where R: gfx::Resources
{
	if let Some((i,_)) = store.iter().enumerate().find(|(_,(idx,_))| *idx==*index)
	{ *index = i }
}

fn load_gltf_node<R>(
	fact: &impl gfx::traits::FactoryExt<R>,
	dir: &Path,
	node: gltf::Node,
	buffers: &[gltf::buffer::Data],
	transform: Matrix4<f32>,
	vertices: &mut Vec<Vertex3NT>,
	indices: &mut Vec<u32>,
) -> Node<R>
	where R: gfx::Resources
{
	let transform = transform * Matrix4::from(node.transform().matrix());

	let start = indices.len();
	let vstart = vertices.len() as u32;
	let mut node_tex = WHITE;
	let mut node_nmap = None;
	let mut node_mrmap = BLACK;
	let mut node_aomap = WHITE;
	let mut node_emmap = BLACK;
	let mut node_mat = usize::max_value();

	if let Some(mesh) = node.mesh()
	{
		for prim in mesh.primitives()
		{
			let material = prim.material();
			if let Some(idx) = material.index()
			{ node_mat = idx}
			let pbr = material.pbr_metallic_roughness();

			if let Some(tex) = pbr.base_color_texture()
			{ node_tex = tex.texture().index() }
			node_nmap = material.normal_texture().map(|tex| tex.texture().index());
			if let Some(tex) = pbr.metallic_roughness_texture()
			{ node_mrmap = tex.texture().index() }
			if let Some(tex) = material.occlusion_texture()
			{ node_aomap = tex.texture().index() }
			if let Some(tex) = material.emissive_texture()
			{ node_emmap = tex.texture().index() }

			let reader = prim.reader(|buff| Some(&buffers[buff.index()]));
			//vertices.append(&mut reader.read_positions().unwrap().map(|pos| Vertex::from(pos)).collect());
			indices.append
			(
				&mut reader
					.read_indices().unwrap()
					.into_u32()
					.map(|i| i + vstart)
					.collect(),
			);
			let positions = reader.read_positions().unwrap();
			let normals = reader.read_normals().unwrap();
			let uv = reader.read_tex_coords(0);
			if let Some(uv) = uv
			{
				let uv = uv.into_f32();
				let iterator = positions.zip(normals.zip(uv));
				for (pos, (norm, tex)) in iterator
				{
					vertices.push(transform *
							Vertex3NT
							{
								pos,
								norm,
								tex: [tex[0], 1.0 - tex[1]],
							},
					);
				}
			}
			else
			{
				let iterator = positions.zip(normals);
				for (pos, norm) in iterator
				{
					vertices.push(transform *
						Vertex3NT
						{
							pos,
							norm,
							tex: [0.0, 0.0],
						},
					);
				}
			}
		}
	}
	let end = indices.len();

	Node
	{
		slice: gfx::Slice
		{
			start: start as u32,
			end: if end==start{0}else{end as u32},
			base_vertex: 0,
			instances: None,
			// put an null buffer (drawn as infinite range)
			// will be replaced by the actual buffer
			// after `indices` has been filled by every node
			buffer: gfx::IndexBuffer::Auto
		},
		children: node.children()
			.map(|child| load_gltf_node(fact, dir, child, buffers, transform, vertices, indices))
			.collect(),
		material: node_mat,
		texture: node_tex,
		nmap: node_nmap,
		mrmap: node_mrmap,
		aomap: node_aomap,
		emissmap: node_emmap,
	}
}

impl<'a> From<gltf::Material<'a>> for PbrMaterial
{
	fn from(mat: gltf::Material) -> Self
	{
		let pbr = mat.pbr_metallic_roughness();
		Self
		{
			albedo_factor: pbr.base_color_factor(),
			roughness_factor: pbr.roughness_factor(),
			metallic_factor: pbr.metallic_factor(),
			//emissive_factor: mat.emissive_factor(),
			occlusion_factor: if let Some(tex) = mat.occlusion_texture()
			{tex.strength()} else { 1.0 },
			alpha_mode: mat.alpha_mode().into(),
			//..Default::default()
		}
	}
}

impl From<gltf::material::AlphaMode> for AlphaMode
{
	fn from(mode: gltf::material::AlphaMode) -> Self
	{
		use self::AlphaMode::*;
		match mode
		{
			gltf::material::AlphaMode::Opaque => Opaque,
			gltf::material::AlphaMode::Mask => Mask,
			gltf::material::AlphaMode::Blend => Blend,
		}
	}
}

pub(crate) fn load_gltf<R, S>(
	fact: &mut impl gfx::traits::FactoryExt<R>,
	path: S,
	scene_idx: usize,
	transform: Matrix4<f32>,
) -> NodeHierarchy<R>
	where
        R: gfx::Resources,
        S: AsRef<Path>,
{
	let (gltf, buffers, _) = gltf::import(path.as_ref()).unwrap();
	println!("GLTF file loaded");
	let scene = gltf.scenes().nth(scene_idx);
	//let dir = Path::new(&path).parent().unwrap();
    let dir = path.as_ref().parent().unwrap();

	let scene = scene.unwrap();
	let mut vertices = vec![];
	let mut indices = vec![];

	println!("Loading textures and materials");

	let kind = gfx::texture::Kind::D2(1, 1, gfx::texture::AaMode::Single);
	let white = fact.create_texture_immutable_u8::<Rgba8>(kind, gfx::texture::Mipmap::Provided, &[&[255,255,255,255]]).unwrap().1;
	let black = fact.create_texture_immutable_u8::<Rgba8>(kind, gfx::texture::Mipmap::Provided, &[&[0,0,0,255]]).unwrap().1;
	let mut textures = vec![(usize::max_value(),black), (usize::max_value()-1, white)];
	let mut materials = vec![];
	println!("Default textures loaded");

	for mat in gltf.materials()
	{
		let pbr = mat.pbr_metallic_roughness();
		load_gltf_texture::<Srgba8,_,_,_>(fact, dir, pbr.base_color_texture(), &mut textures);
		load_gltf_texture::<Rgba8,_,_,_>(fact, dir, mat.normal_texture(), &mut textures);
		load_gltf_texture::<Rgba8,_,_,_>(fact, dir, mat.occlusion_texture(), &mut textures);
		load_gltf_texture::<Srgba8,_,_,_>(fact, dir, mat.emissive_texture(), &mut textures);
		load_gltf_texture::<Rgba8,_,_,_>(fact, dir, pbr.metallic_roughness_texture(), &mut textures);
		/*let pbr = mat.pbr_metallic_roughness();
		if pbr.base_color_texture().is_none()
		{
			let img = [255,0,0,255];
			let kind = gfx::texture::Kind::D2(1, 1, gfx::texture::AaMode::Single);
			textures.push(fact.create_texture_immutable_u8::<gfx::format::Rgba8>(kind, gfx::texture::Mipmap::Provided, &[&img]).unwrap().1);
		}*/
		materials.push(mat.into());
	}

	println!("Loading geometry");

	let mut nodes: Vec<_> = scene.nodes().map(|node|
	{
		load_gltf_node(fact, dir, node, &buffers, transform, &mut vertices, &mut indices)
	}).collect();
	println!("Uploading geometry");
	let (vbuf,slice) = fact.create_vertex_buffer_with_slice(&vertices[..], &indices[..]);

	let mut nh = NodeHierarchy
	{
		nodes: vec!(),
		vbuf,
		ibuf: slice.buffer.clone(),
		textures,
		materials,
		loaded_texture: usize::max_value(),
		loaded_material: usize::max_value(),
	};

	for n in &mut nodes
	{
		n.update_tex_indices(&nh);
		n.update_idx_buffer(&slice.buffer);
	}
	nh.nodes = nodes;
	println!("Done");

	nh
}
