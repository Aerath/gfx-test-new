mod gltf;
#[macro_use]
mod helper;

use std::time::{Duration,Instant};

//use rand::Rng;
//use glutin::Window;
use gfx;
use gfx::{gfx_constant_struct_meta,gfx_defines,gfx_impl_struct_meta,gfx_pipeline,gfx_pipeline_inner};
use gfx::{Device,Slice,traits::{FactoryExt},format::{Rgba8,Rgba16F,Float,R16_G16,R16_G16_B16,Srgba8,Unorm}};
use gfx::handle::RenderTargetView;
use gfx_window_glutin as gfx_glutin;
use cgmath::{Matrix4,Vector4,perspective,Deg,Point3,Vector3,ortho};
use crate::gltf::{load_gltf,Ppline,PbrMaterial,NodeHierarchy};
use crate::helper::{load_tex,load_shader,PipelineObj,SQUARE,Vertex2,Vertex3NT,load_cubemap,CUBE_V,CUBE_I,Light,LightType};

type Rgb16F = (R16_G16_B16, Float);
type ScreenFormat = gfx::format::Rgba8;
type DepthFormat = gfx::format::Depth;
/*const ColKind:gfx::texture::Kind = gfx::texture::Kind::D2(SCREEN_SIZE.0, SCREEN_SIZE.1, gfx::texture::AaMode::Single);
const GeomKind:gfx::texture::Kind = gfx::texture::Kind::D2(SCREEN_SIZE.0, SCREEN_SIZE.1, gfx::texture::AaMode::Single);
const ShadKind:gfx::texture::Kind = gfx::texture::Kind::D2(2048, 2048, gfx::texture::AaMode::Single);*/

const LIGHTS_NUM: usize = 4;

const CAM_SLOW:f32 = 0.5;
const CAM_NORM:f32 = 1.0;
const CAM_FAST:f32 = 2.0;

#[derive(Clone,Debug)]
struct Settings
{
	screen_size: (u16,u16),
	fov: u8,
    model: String,
    scale: f32,
	framerate: u8,
	updates: u8,
	mouse_speed: f32,
    relative_input: bool,
    demo_mode: bool,
}

#[derive(Copy,Clone,Debug)]
struct State
{
	running: bool,
	light_moving: bool,
	cam_speed: f32,
	cam_pos: Point3<f32>,
	cam_dir: Vector4<f32>,
	yaw: f32,
	pitch: f32,
	proj: Matrix4<f32>,
	last_frame: Instant,
	last_update: Instant,
	fcounter: u64,
	debug: i32,
    // Mouse delta-positionning doesn't work on VMs and Windows X11 servers (returning absolute
    // values instead), it is necessary to keep track of it manually
    prev_mouse: (f64, f64),
}
#[derive(Clone,Debug)]
struct Slices<R>
	where R: gfx::Resources
{
	sky: Slice<R>,
	square: Slice<R>
}
#[derive(Clone,Debug)]
struct Pipelines<R>
	where R: gfx::Resources
{
	shadow: PipelineObj<R,shadow::Data<R>>,
	deffered1: PipelineObj<R,deffered1::Data<R>>,
	deffered2: PipelineObj<R,deffered2::Data<R>>,
	skyplo: PipelineObj<R,skypipe::Data<R>>,
	pproc: PipelineObj<R,postprocess::Data<R>>,
	gausses: (PipelineObj<R,gauss::Data<R>>,PipelineObj<R,gauss::Data<R>>),
}
#[derive(Clone,Debug)]
struct World<R>
	where R: gfx::Resources
{
	models: Vec<NodeHierarchy<R>>,
	lights: [Light;LIGHTS_NUM],
}
/*struct AppData<R,F>
	where R: gfx::Resources, F: gfx::Factory<R>
{
	win: Window,
	fact: F,
	evloop: glutin::EventsLoop,

	settings: Settings,
	state: State,

	world: World<R>,

	pplines: Pipelines<R>,
	slices: Slices<R>
}*/

macro_rules! intoP3
{
	($a:expr) => {Point3{x:$a[0],y:$a[1],z:$a[2]}}
}

macro_rules! buffer_chain
{
	($buf:ident, $($pipe1:ident:$n1:ident),+ ; $($pipe2:ident:$n2:ident),+) =>
	{
		$($pipe1.data.$n1 = $buf.2.clone();)+
		$($pipe2.data.$n2.0 = $buf.1.clone();)+
	}
}

macro_rules! create_gbuffer
{
	($fact:expr, $size:expr) =>
	{
		{
			let depth = $fact.create_depth_stencil_view_only($size.0, $size.1).unwrap();
			let colmap = $fact.create_render_target($size.0, $size.1).unwrap();
			let posmap = $fact.create_render_target($size.0, $size.1).unwrap();
			let normmap = $fact.create_render_target($size.0, $size.1).unwrap();
			let mromap = $fact.create_render_target($size.0, $size.1).unwrap();
			let emmap = $fact.create_render_target($size.0, $size.1).unwrap();

			(depth, colmap, posmap, normmap, mromap, emmap)
		}
	};
	($fact:ident, $pipe1:ident, $pipe2:ident, $size:expr) =>
	{
		let depth = $fact.create_depth_stencil_view_only($size.0, $size.1).unwrap();
		let colmap = $fact.create_render_target($size.0, $size.1).unwrap();
		let posmap = $fact.create_render_target($size.0, $size.1).unwrap();
		let normmap = $fact.create_render_target($size.0, $size.1).unwrap();
		let mromap = $fact.create_render_target($size.0, $size.1).unwrap();
		let emmap = $fact.create_render_target($size.0, $size.1).unwrap();

		$pipe1.data.depth = depth;

		buffer_chain!(colmap, $pipe1:colmap ; $pipe2:colmap);
		buffer_chain!(posmap, $pipe1:posmap ; $pipe2:posmap);
		buffer_chain!(normmap, $pipe1:normmap ; $pipe2:normmap);
		buffer_chain!(mromap, $pipe1:mromap ; $pipe2:mromap);
		buffer_chain!(emmap, $pipe1:emmap ; $pipe2:emmap);
	}
}

gfx_defines!
{
	/*vertex Vertex2
	{
		pos: [f32;2] = "pos",
	}
	vertex Vertex3NT
	{
		pos: [f32;3] = "pos",
		norm: [f32;3] = "norm",
		tex: [f32;2] = "tex",
	}*/

	constant Transform
	{
		vp: [[f32;4];4] = "vp",
		model: [[f32;4];4] = "model",
	}

	constant ViewProj
	{
		proj: [[f32;4];4] = "proj",
		view: [[f32;4];4] = "view",
	}

	constant NTextured
	{
		ntextured: bool = "ntextured",
	}

	pipeline shadow
	{
		vbuf: gfx::VertexBuffer<Vertex3NT> = (),
		transform: gfx::ConstantBuffer<Transform> = "Transform",
		shmap: gfx::DepthTarget<DepthFormat> = gfx::preset::depth::LESS_EQUAL_WRITE,
	}

	pipeline skypipe
	{
		vbuf: gfx::VertexBuffer<Vertex3NT> = (),
		view_proj: gfx::ConstantBuffer<ViewProj> = "ViewProj",
		skytex: gfx::TextureSampler<[f32;4]> = "cubemap",
		main_col: gfx::RenderTarget<Rgba16F> = "main_col",
		depth: gfx::DepthTarget<DepthFormat> = gfx::preset::depth::LESS_EQUAL_WRITE,
	}

	pipeline deffered1
	{
		vbuf: gfx::VertexBuffer<Vertex3NT> = (),
		coltex: gfx::TextureSampler<[f32;4]> = "coltex",
		normtex: gfx::TextureSampler<[f32;4]> = "normtex",
		mrtex: gfx::TextureSampler<[f32;4]> = "mrtex",
		aotex: gfx::TextureSampler<[f32;4]> = "aotex",
		emtex: gfx::TextureSampler<[f32;4]> = "emtex",
		transform: gfx::ConstantBuffer<Transform> = "Transform",
		material: gfx::ConstantBuffer<PbrMaterial> = "PbrMaterial",
		ntextured: gfx::ConstantBuffer<NTextured> = "NTextured",

		colmap: gfx::RenderTarget<Rgba8> = "colmap",
		posmap: gfx::RenderTarget<Rgb16F> = "posmap",
		/*normmap: gfx::RenderTarget<(R8_G8_B8_A8,Inorm)> = "normmap",
		mromap: gfx::RenderTarget<(R5_G6_B5,Unorm)> = "mromap",*/
		normmap: gfx::RenderTarget<Rgb16F> = "normmap",
		mromap: gfx::RenderTarget<Rgb16F> = "mromap",
		emmap: gfx::RenderTarget<Rgba8> = "emmap",
		depth: gfx::DepthTarget<DepthFormat> = gfx::preset::depth::LESS_EQUAL_WRITE,
	}

	constant Constants
	{
		lspace: [[f32;4];4] = "lspace",
		camera: [f32;3] = "camera",
		debug: i32 = "debug",
	}

	pipeline deffered2
	{
		vbuf: gfx::VertexBuffer<Vertex2> = (),
		colmap: gfx::TextureSampler<[f32;4]> = "colmap",
		posmap: gfx::TextureSampler<[f32;3]> = "posmap",
		normmap: gfx::TextureSampler<[f32;3]> = "normmap",
		mromap: gfx::TextureSampler<[f32;3]> = "mromap",
		emmap: gfx::TextureSampler<[f32;4]> = "emmap",
		//shmap: gfx::TextureSampler<DepthFormat> = "shmap",
		shmap: gfx::TextureSampler<f32> = "shmap",
		brdf_map: gfx::TextureSampler<[f32;2]> = "brdf_int",
		irrmap: gfx::TextureSampler<[f32;4]> = "irrmap",
		//envmap: gfx::TextureSampler<[f32;3]> = "envmap",
		lights: gfx::ConstantBuffer<Light> = "Lights",
		consts: gfx::ConstantBuffer<Constants> = "Constants",

		main_out: gfx::RenderTarget<Rgba16F> = "main_out",
		hdr: gfx::RenderTarget<Rgba16F> = "hdr",
	}

	pipeline gauss
	{
		vbuf: gfx::VertexBuffer<Vertex2> = (),
		src: gfx::TextureSampler<[f32;4]> = "src",
		dst: gfx::RenderTarget<Rgba16F> = "dst",
	}

	pipeline postprocess
	{
		vbuf: gfx::VertexBuffer<Vertex2> = (),
		main_col: gfx::TextureSampler<[f32;4]> = "main_col",
		hdr: gfx::TextureSampler<[f32;4]> = "hdr",
		screen: gfx::RenderTarget<ScreenFormat> = "screen",
	}
}

fn main()
{
	let settings = Settings::from_args();

	let mut evloop = glutin::EventsLoop::new();
	let (win, mut dev, mut fact, col_view, _depth_view) =
	{
		let winbuilder = glutin::WindowBuilder::new()
			.with_title("GFX test")
			.with_maximized(true)
			.with_dimensions((settings.screen_size.0 as u32, settings.screen_size.1 as u32 + 23).into());
		let conbuilder = glutin::ContextBuilder::new()
			.with_gl_profile(glutin::GlProfile::Core)
			.with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (3,3)))
			.with_srgb(false);

		gfx_glutin::init::<ScreenFormat, DepthFormat>(winbuilder, conbuilder, &evloop).unwrap()
	};
	/*unsafe
	{
		extern crate gfx_gl;
		dev.with_gl(|gl|{ gl.Disable(gfx_gl::FRAMEBUFFER_SRGB) });
	}*/

	let mut encoder: gfx::Encoder<_,_> = fact.create_command_buffer().into();
	let (mut pplines,slices) = Pipelines::new_with_slices(&mut fact, &mut encoder, &settings, col_view);
	encoder.flush(&mut dev);

	//let model = load_gltf(&mut fact, "assets/models/9_mm/scene.gltf", 0, Matrix4::from_scale(0.02f32));
	//let model = load_gltf(&mut fact, "assets/models/guidoor/scene.gltf", 0, Matrix4::from_scale(0.005f32));
	let model = load_gltf(&mut fact, &settings.model, 0, Matrix4::from_scale(settings.scale));
	println!("{:#?}", model);

    if settings.demo_mode
    { println!(" --- DEMO MODE ---") }

	println!("Done loading");

	let mut world = World
	{
		models: vec!(model),
		lights:
		[
			Light{ltype: LightType::Directional, ..Default::default()},
			Light{diffuse:[1.0,0.0,0.0,1.0], pos: [3.0,0.2,3.0, 0.0], ltype: LightType::Point, ..Default::default()},
			Light{diffuse:[0.0,1.0,0.0,1.0], pos: [0.0,0.2,3.0, 0.0], ltype: LightType::Point, ..Default::default()},
			Light{diffuse:[0.0,0.0,1.0,1.0], pos: [-3.0,0.2,3.0, 0.0], ltype: LightType::Point, ..Default::default()},
		],
	};
	let mut state = State
	{
		cam_pos: Point3::new(-3.0, 3.0, 3.0),
		//cam_dir: Vector4::new(1.0, -0.5, -1.0, 0.2),
		proj: perspective(Deg(settings.fov as f32), settings.screen_size.0 as f32/settings.screen_size.1 as f32, 0.1f32, 100f32),
		.. Default::default()
	};
    if settings.demo_mode
    {
        state.cam_pos = Point3::new(2.0, 1.0, -2.0);
    }
    state.cam_dir = -state.cam_pos.to_homogeneous();

	/*let mut data = AppData
	{
		win,
		fact,
		evloop,
		world,
		settings,
		state,
		pplines,
		slices,
	};*/

	/*let (ctxt_tx,draw_rx) = std::sync::mpsc::channel();
	let draw_thr = std::thread::spawn(move ||
	{
		use std::sync::mpsc::TryRecvError;
		loop
		{
			match draw_rx.try_recv()
			{
				Ok((encoder,world)) => draw(&mut encoder, &state, &world, &mut pplines, &slices),
				Err(TryRecvError::Empty) => {}
				Err(TryRecvError::Disconnected) => break,
			}
			std::thread::sleep(Duration::from_millis(1000/settings.framerate as u64 - 1));
		}
	});*/

	/*let update_thr = std::thread::spawn(move ||
	{
		use std::sync::mpsc::TryRecvError;

		let mut evloop = evloop;
		loop
		{
			input(&mut evloop, &mut state, &settings);
			update(&mut state, &mut world);
			std::thread::sleep(Duration::from_millis(1000 / settings.updates as u64 - 1));
			match rx.try_recv()
			{
				Ok(_) | Err(TryRecvError::Disconnected) => break,
				Err(TryRecvError::Empty) => {}
			}
		}
	});*/

	while state.running
	{
		input(&win, &mut evloop, &mut state, &settings);
		if !settings.demo_mode
        { update(&mut state, &mut world) }
		//ctxt_tx.send((encoder.clone(),world.clone()));
		draw(&mut encoder, &state, &world, &mut pplines, &slices);
		encoder.flush(&mut dev);
		win.swap_buffers().unwrap();
		dev.cleanup();
        if !settings.demo_mode
		{ std::thread::sleep(Duration::from_millis(1000/settings.framerate as u64 -1)) }
	}
	println!("Killing draw thread");
	// Close the channel to end draw loop
	//drop(ctxt_tx);
	println!("Waiting for draw thread end");
	//draw_thr.join();
	//update_thr.join();
}

fn input(win: &glutin::ContextWrapper<glutin::PossiblyCurrent,glutin::Window>, evloop: &mut glutin::EventsLoop, state: &mut State, settings: &Settings)
{
	/*let pplines = &mut data.pplines;
	let fact = &mut data.fact;
	let win = &mut data.win;*/

	evloop.poll_events(|ev| match ev
	{
		glutin::Event::WindowEvent{event,..} =>
		{
			use glutin::WindowEvent::*;
			use glutin::VirtualKeyCode;
			use glutin::ElementState;
			match event
			{
				CloseRequested => state.running = false,
				/*Resized(size) =>
				{
					let size = (size.width as u16, size.height as u16);

					settings.screen_size = size;
					state.proj = perspective(Deg(settings.fov as f32), size.0 as f32/size.1 as f32, 0.1f32, 100f32);

					let deffered1 = &mut pplines.deffered1;
					let deffered2 = &mut pplines.deffered2;
					let skyplo = &mut pplines.skyplo;
					let (depth,colmap,posmap,normmap,mromap,emmap) = create_gbuffer!(fact, size);
					deffered1.data.depth = depth;
					skyplo.data.depth = deffered1.data.depth.clone();
					buffer_chain!(colmap, deffered1:colmap ; deffered2:colmap);
					buffer_chain!(posmap, deffered1:posmap ; deffered2:posmap);
					buffer_chain!(normmap, deffered1:normmap ; deffered2:normmap);
					buffer_chain!(mromap, deffered1:mromap ; deffered2:mromap);
					buffer_chain!(emmap, deffered1:emmap ; deffered2:emmap);

					let pproc = &mut pplines.pproc;
					let (gauss1,gauss2) = &mut pplines.gausses;
					let main_col = fact.create_render_target(size.0, size.1).unwrap();
					buffer_chain!(main_col, deffered2:main_out, skyplo:main_col ; pproc:main_col);
					let hdr = fact.create_render_target(size.0, size.1).unwrap();
					buffer_chain!(hdr, deffered2:hdr, gauss2:dst ; gauss1:src, pproc:hdr);

					let gauss_buf = fact.create_render_target(size.0, size.1).unwrap();
					buffer_chain!(gauss_buf, gauss1:dst ; gauss2:src);
					let screen: RenderTargetView<_,ScreenFormat> = gfx_glutin::new_views::<_,DepthFormat>(win).0;
					pproc.data.screen = screen;
					//pproc.data.screen = gfx_glutin::update_views(&data.win, &mut data.pproc.screen).0;
				},*/
				KeyboardInput{input,..} => if let (false, Some(keycode)) = (settings.demo_mode, input.virtual_keycode)
				{
					//println!("{:?} {:?}", input.state, keycode);
					if input.state == ElementState::Pressed
					{
						let cam_speed = 0.3f32;
						match keycode
						{
							VirtualKeyCode::A | VirtualKeyCode::E =>
							{
								state.debug += match keycode
								{
									VirtualKeyCode::A => 1,
									VirtualKeyCode::E => -1,
									_ => 0
								};
								match state.debug
								{
									7 => state.debug = 0,
									-1 => state.debug = 6,
									_ => {}
								};

								let debug_state = match state.debug
								{
									0 => "None",
									1 => "D",
									2 => "F",
									3 => "G",
									4 => "denom",
									5 => "G/denom",
									6 => "emiss+ambient",
									_ => ""
								};
								println!("debug:{}", debug_state);
								win.window().set_title(&format!("GFX test - debug: {}", debug_state));
							},
							VirtualKeyCode::P => state.light_moving = !state.light_moving,
							VirtualKeyCode::Z => state.cam_pos += cam_speed*state.cam_speed * state.cam_dir.truncate(),
							VirtualKeyCode::Q => state.cam_pos += cam_speed*state.cam_speed * Vector3::new(state.cam_dir.z, 0.0, -state.cam_dir.x),
							VirtualKeyCode::S => state.cam_pos += cam_speed*state.cam_speed * -state.cam_dir.truncate(),
							VirtualKeyCode::D => state.cam_pos += cam_speed*state.cam_speed * Vector3::new(-state.cam_dir.z, 0.0, state.cam_dir.x),
							VirtualKeyCode::Escape => state.running = false,
							VirtualKeyCode::LShift => state.cam_speed = CAM_FAST,
							VirtualKeyCode::LControl => state.cam_speed = CAM_SLOW,
							_ => {}
						}
					}
					else
					{
						if input.state == ElementState::Released
						{
							match keycode
							{
								VirtualKeyCode::LShift | VirtualKeyCode::LControl => state.cam_speed = CAM_NORM,
								_ => {}
							}
						}
					}
				}
				_ => {}
			}
		}
        // Should be dx,dy but as explained above, delta-positionning doesnt always work
		glutin::Event::DeviceEvent{event,..} => if let (false, glutin::DeviceEvent::MouseMotion{delta:(ix,iy)}) =  (settings.demo_mode, event)
		{
            let dx: f64;
            let dy: f64;
            if settings.relative_input
            {
                dx = ix;
                dy = iy;
            }
            else
            {
                dx = ix - state.prev_mouse.0;
                dy = iy - state.prev_mouse.1;
                state.prev_mouse = (ix, iy);
            }

			state.yaw += dx as f32 * settings.mouse_speed;
			state.pitch -= dy as f32 * settings.mouse_speed;
			if state.pitch > 1.55 { state.pitch = 1.55 }
			if state.pitch < -1.55 { state.pitch = -1.55 }
			state.cam_dir.x = state.pitch.cos() * state.yaw.cos();
			state.cam_dir.y = state.pitch.sin();
			state.cam_dir.z = state.pitch.cos() * state.yaw.sin();
		},
		_ => {}
	});
}

fn update<R>(state: &mut State, world: &mut World<R>)
	where R: gfx::Resources
{
	state.fcounter += 1;
	if state.fcounter == std::u64::MAX
	{state.fcounter = 0}

	let now = Instant::now();
	let delta = now - state.last_update;
	state.fcounter += (delta.as_secs()*1_000 + delta.subsec_millis()as u64) / 15;
	let counter = state.fcounter;

	if state.light_moving
	{
		let light = &mut world.lights[0];
		light.pos = [2.0*f32::cos(counter as f32*0.003), 0.8+0.2*f32::cos((counter%120)as f32*0.0523), 2.0*f32::sin((counter%3600) as f32*0.003), 0.0];
		light.direction = [-light.pos[0],-light.pos[0],-light.pos[0], 0.0];
	}

	state.last_update = now;
}

fn draw<R,B>(encoder: &mut gfx::Encoder<R,B>, state: &State, world: &World<R>, pplines: &mut Pipelines<R>, slices: &Slices<R>)
	where R: gfx::Resources, B: gfx::CommandBuffer<R>
{
	let cam_pos = state.cam_pos;
	let cam_dir = state.cam_dir;
	let deffered1 = &pplines.deffered1;
	let deffered2 = &pplines.deffered2;

	let view = Matrix4::look_at(cam_pos, cam_pos+cam_dir.truncate(), Vector3::unit_y());
	let view_proj = ViewProj
	{
		view: view.into(),
		proj: state.proj.into(),
	};
	encoder.update_buffer(&pplines.skyplo.data.view_proj, &[view_proj], 0).unwrap();
	let transform = Transform
	{
		vp: (state.proj*view).into(),
		.. Default::default()
	};
	encoder.update_buffer(&deffered1.data.transform, &[transform], 0).unwrap();

	encoder.update_buffer(&deffered2.data.lights, &world.lights, 0).unwrap();
	let view = Matrix4::look_at([world.lights[0].pos[0], world.lights[0].pos[1], world.lights[0].pos[2]].into(), Point3::new(0.0,0.0,0.0), Vector3::unit_y());
	let proj = ortho(-5.0, 5.0, -3.0, 3.0, 0.5, 10.0);
	let def2consts = Constants
	{
		lspace:
		{
			//let proj = perspective(Deg(90f32), WDIMS.0 as f32/WDIMS.1 as f32, 0.9f32, 80f32,);
			proj*view
		}.into(),
		camera: [cam_pos.x, cam_pos.y, cam_pos.z],
		debug: state.debug,
	};
	encoder.update_buffer(&deffered2.data.consts, &[def2consts], 0).unwrap();

	let shadow = &mut pplines.shadow;
	encoder.clear_depth(&shadow.data.shmap, 1.0);
	let sh_transf =
	{
		let view = Matrix4::look_at(intoP3!(world.lights[0].pos), Point3::new(0.0,0.0,0.0), Vector3::unit_y());
		let proj = ortho(-5.0, 5.0, -3.0, 3.0, 0.5, 10.0);

		Transform
		{
			vp: (proj*view).into(),
			.. Default::default()
		}
	};
	encoder.update_buffer(&shadow.data.transform, &[sh_transf], 0).unwrap();
	for model in &world.models
	{ model.draw(encoder, Ppline::Shad(shadow)) }

	encoder.clear(&deffered1.data.colmap, [0f32,0.0,0.0,0.0]);
	encoder.clear_depth(&deffered1.data.depth, 1.0);
	for model in &world.models
	{ model.draw(encoder, Ppline::Def1(&mut pplines.deffered1))}

	encoder.draw(&slices.sky, &pplines.skyplo.state, &pplines.skyplo.data);

	encoder.clear(&pplines.deffered2.data.hdr, [0f32,0.0,0.0,0.0]);
	encoder.draw(&slices.square, &pplines.deffered2.state, &pplines.deffered2.data);

	/*for _ in 0..0
	{
		encoder.draw(&slices.square, &pplines.gausses.0.state, &pplines.gausses.0.data);
		encoder.draw(&slices.square, &pplines.gausses.1.state, &pplines.gausses.1.data);
	}*/

	encoder.draw(&slices.square, &pplines.pproc.state, &pplines.pproc.data);
}

impl<R> Pipelines<R>
	where R: gfx::Resources
{
	//fn new_with_slices<F,Rf>(fact: &mut F, settings: &Settings, col_view: RenderTargetView<R,Rf>) -> (Self,Slices<R>)
	fn new_with_slices<F,C>(fact: &mut F, _encoder: &mut gfx::Encoder<R,C>, settings: &Settings, col_view: RenderTargetView<R,ScreenFormat>) -> (Self,Slices<R>)
		where F: gfx::Factory<R>, C: gfx::CommandBuffer<R>
	{
		let sampler = fact.create_sampler_linear();

		let fake_tex =
		{
			let fake_img = [255,255,255,255];
			let kind = gfx::texture::Kind::D2(1, 1, gfx::texture::AaMode::Single);
			let view = fact.create_texture_immutable_u8::<Rgba8>(kind, gfx::texture::Mipmap::Provided, &[&fake_img]).unwrap().1;
			(view, sampler.clone())
		};

		let shmap = fact.create_depth_stencil::<DepthFormat>(2048, 2048).unwrap();
		let set = fact.create_shader_set(load_shad!("assets/shaders/simple.vert", "NO_OUT"), load_shad!("assets/shaders/empty.glsl")).expect("Failed to compile shadow program");
		let rasterizer = gfx::state::Rasterizer
		{
			cull_face: gfx::state::CullFace::Front,
			..gfx::state::Rasterizer::new_fill()
		};
		let shadow = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleList, rasterizer, shadow::new()).expect("Failed to link shadow program"),
			data: shadow::Data
			{
				vbuf: fact.create_vertex_buffer(&[]),
				transform: fact.create_constant_buffer(1),
				shmap: shmap.2,
			}
		};
		println!("Pipeline compiled");

		let (depth, colmap, posmap, normmap, mromap, emmap) = create_gbuffer!(fact, settings.screen_size);
		let set = fact.create_shader_set(load_shad!("assets/shaders/simple.vert"), load_shad!("assets/shaders/def1.frag", "PBR")).expect("Failed to compile def1 program");
		let rasterizer = gfx::state::Rasterizer::new_fill().with_cull_back();
		let deffered1 = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleList, rasterizer, deffered1::new()).expect("Failed to link def1 program"),
			data: deffered1::Data
			{
				vbuf: shadow.data.vbuf.clone(),
				transform: fact.create_constant_buffer(1),
				material: fact.create_constant_buffer(1),
				ntextured: fact.create_constant_buffer(1),
				coltex: fake_tex.clone(),
				normtex: fake_tex.clone(),
				mrtex: fake_tex.clone(),
				aotex: fake_tex.clone(),
				emtex: fake_tex,
				colmap: colmap.2,
				posmap: posmap.2,
				normmap: normmap.2,
				mromap: mromap.2,
				emmap: emmap.2,
				depth: depth.clone(),
			}
		};
		println!("Pipeline compiled");

		let main_col = fact.create_render_target(settings.screen_size.0, settings.screen_size.1).unwrap();

		let (vbuf,skyslice) = fact.create_vertex_buffer_with_slice(&CUBE_V, &CUBE_I[..]);
		let skytexes = ["ft","bk","up","dn","rt","lf"].iter().map(|dir| format!("assets/textures/skyboxes/hw_lagoon/lagoon_{}.tga", dir));
		let skyplo = PipelineObj
		{
			state: fact.create_pipeline_simple(load_shad!("assets/shaders/skybox.vert"), load_shad!("assets/shaders/skybox.frag"), skypipe::new())
				.expect("Failed to compile skybox program"),
				data: skypipe::Data
				{
					vbuf,
					view_proj: fact.create_constant_buffer(1),
					skytex: (load_cubemap::<Srgba8,_,_,_,_>(fact, skytexes).1, sampler.clone()),
					main_col: main_col.2.clone(),
					depth,
				}
		};
		println!("Pipeline compiled");

		let (square_vbuf,sq_slice) = fact.create_vertex_buffer_with_slice(&SQUARE, ());
		let hdr = fact.create_render_target(settings.screen_size.0, settings.screen_size.1).unwrap();
		let irrtexes = ["ft","bk","up","dn","rt","lf"].iter().map(|dir| format!("assets/textures/skyboxes/hw_lagoon/convoluted_{}.png", dir));
		let irrmap = load_cubemap::<Rgba8,_,_,_,_>(fact, irrtexes).1;
		//let irrmap = convolute_irrmap(fact, encoder, &skyplo.data.skytex.0, 512);
		let brdf_int = load_tex::<(R16_G16,Unorm),_,_,_>(fact, "assets/textures/brdf_intmap.png");
		/*let mut encoder: gfx::Encoder<_,_> = fact.create_command_buffer().into();
		let brdf_int = integrate_brdf(fact, encoder, 512);
		encoder.flush();*/
		let minim_vert = load_shad!("assets/shaders/minim.vert");
		let set = fact.create_shader_set(minim_vert, load_shad!("assets/shaders/def2.frag",
				//"TONE_MAP reinhard_tone_map","HDR","PBR", "IBL","SHADOWS",&format!("LIGHTS_NUM {}",LIGHTS_NUM)[..])).expect("Failed to compile def2 program");
				"GAMMA_CORR 2.2", "TONE_MAP expos_tone_map","HDR","PBR","SHADOWS",&format!("LIGHTS_NUM {}",LIGHTS_NUM)[..])).expect("Failed to compile def2 program");
		let deffered2 = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleStrip, gfx::state::Rasterizer::new_fill(), deffered2::new()).expect("Failed to link def2 program"),
			data: deffered2::Data
			{
				vbuf: square_vbuf.clone(),
				colmap: (colmap.1, sampler.clone()),
				posmap: (posmap.1, sampler.clone()),
				normmap: (normmap.1, sampler.clone()),
				mromap: (mromap.1, sampler.clone()),
				emmap: (emmap.1, sampler.clone()),
				shmap: (shmap.1, sampler.clone()),
				irrmap: (irrmap, sampler.clone()),
				brdf_map: (brdf_int.1, sampler.clone()),
				//envmap: skyplo.data.skytex.clone(),

				lights: fact.create_constant_buffer(LIGHTS_NUM),
				consts: fact.create_constant_buffer(1),

				main_out: main_col.2,
				hdr: hdr.2.clone(),
			}
		};
		println!("Pipeline compiled");

		/*let mut rng = rand::thread_rng();
		let ssao_samples: Vec<[f32;3]> = (0..64).map(|i|
		{
			let scale = i as f32/64.0;
			let res: [f32;3] = (Vector3::new(
				rng.gen_range(-1f32, 1f32),
				rng.gen_range(-1f32, 1f32),
				rng.gen_range(0f32, 1f32),
				).normalize()
				* rng.gen_range(0f32, 1f32)
				* (0.1+scale*scale*0.9)
				).into();
			res
		}).into();*/

		let set = fact.create_shader_set(minim_vert, load_shad!("assets/shaders/gauss.frag", "HORIZONTAL")).expect("Failed to compile gauss_h program");
		let gauss_buf = fact.create_render_target(settings.screen_size.0/2, settings.screen_size.1/2).unwrap();
		let gauss_h = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleStrip, gfx::state::Rasterizer::new_fill(), gauss::new()).expect("Failed to link gauss_h program"),
			data: gauss::Data
			{
				vbuf: square_vbuf.clone(),
				src: (hdr.1.clone(), sampler.clone()),
				dst: gauss_buf.2.clone(),
			}
		};
		let set = fact.create_shader_set(minim_vert, load_shad!("assets/shaders/gauss.frag", "VERTICAL")).expect("Failed to compile gauss_v program");
		let gauss_v = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleStrip, gfx::state::Rasterizer::new_fill(), gauss::new()).expect("Failed to link gauss_v program"),
			data: gauss::Data
			{
				vbuf: square_vbuf.clone(),
				src: (gauss_buf.1.clone(), sampler.clone()),
				dst: hdr.2.clone(),
			}
		};

		let set = fact.create_shader_set(minim_vert, load_shad!("assets/shaders/post_proc.frag")).expect("Failed to compile post-proc program");
		// mutable to allow changing render target on window resize
		let pproc = PipelineObj
		{
			state: fact.create_pipeline_state(&set, gfx::Primitive::TriangleStrip, gfx::state::Rasterizer::new_fill(), postprocess::new()).unwrap(),
			data: postprocess::Data
			{
				vbuf: deffered2.data.vbuf.clone(),
				main_col: (main_col.1, sampler.clone()),
				hdr: (hdr.1, sampler.clone()),
				screen: col_view
			}
		};
		println!("Pipeline compiled");
		(Pipelines
		{
			pproc,
			skyplo,
			deffered1,
			deffered2,
			shadow,
			gausses: (gauss_h,gauss_v),
		},
		Slices
		{
			sky: skyslice,
			square: sq_slice,
		})
	}

}

impl Default for Constants
{
	fn default() -> Self
	{
		Self
		{
			lspace:[[1.0, 0.0, 0.0, 0.0],
					[0.0, 1.0, 0.0, 0.0],
					[0.0, 0.0, 1.0, 0.0],
					[0.0, 0.0, 0.0, 1.0]],
			camera: [1.0, 1.0, 0.0],
			debug: 0,
		}
	}
}

impl From<bool> for NTextured
{
	fn from(b: bool) -> Self
	{
		Self{ntextured: b}
	}
}

impl Default for ViewProj
{
	fn default() -> Self
	{
		Self
		{
			view:[[1.0, 0.0, 0.0, 0.0],
				  [0.0, 1.0, 0.0, 0.0],
				  [0.0, 0.0, 1.0, 0.0],
				  [0.0, 0.0, 0.0, 1.0]],
			proj:[[1.0, 0.0, 0.0, 0.0],
				  [0.0, 1.0, 0.0, 0.0],
				  [0.0, 0.0, 1.0, 0.0],
				  [0.0, 0.0, 0.0, 1.0]],
		}
	}
}

impl Default for State
{
	fn default() -> Self
	{
		Self
		{
			running: true,
			light_moving: true,
			cam_speed: CAM_NORM,
			cam_pos: Point3::new(0.0,0.0,0.0),
			cam_dir: Vector4::unit_x(),
			yaw: 0.0,
			pitch: 0.0,
			last_frame: Instant::now(),
			last_update: Instant::now(),
			fcounter: 0,
			proj: perspective(Deg(90f32), 16f32/9f32, 0.1f32, 100f32),
			debug: 0,
            prev_mouse: (0.0,0.0),
		}
	}
}

impl Settings
{
    fn from_args() -> Self
    {
        let mut res = Self::default();

        let mut args = std::env::args().skip(1);
        while let Some(arg) = args.next()
        {
            match &arg[..]
            {
                "-d"|"--demo" => res.demo_mode = true,
                //"-e"|"--exposure" => res.exposure = args.next().unwrap().parse().unwrap(),
                "-m"|"--model" => res.model = args.next().unwrap().clone(),
                "-s"|"--scale" => res.scale = args.next().unwrap().parse().unwrap(),
                //"-S"|"--sky" => res.sky = args.next().unwrap().clone(),
                "-f"|"--fov" => res.fov = args.next().unwrap().parse().unwrap(),
                //"-l"|"--lag-mode" => res.lag_mode = true,
                "-h"|"--help" => println!("TODO!"),
                _ => println!("Uknown argument {}", arg)
            }
        }
        res
    }
}

impl Default for Settings
{
	fn default() -> Self
	{
		Self
		{
            model: "assets/models/damagedHelmet/damagedHelmet.gltf".into(),
            scale: 1f32,
			screen_size: (1920, 1057), // Give 23px space for title bar (window size != screen size)
			fov: 45,
			framerate: 60,
			updates: 60,
			mouse_speed: 0.01f32,
            demo_mode: false,
            relative_input: true,
		}
	}
}

impl Default for Transform
{
	fn default() -> Self
	{
		Transform
		{
			vp:   [[1.0, 0.0, 0.0, 0.0],
					[0.0, 1.0, 0.0, 0.0],
					[0.0, 0.0, 1.0, 0.0],
					[0.0, 0.0, 0.0, 1.0]],
			model: [[1.0, 0.0, 0.0, 0.0],
					[0.0, 1.0, 0.0, 0.0],
					[0.0, 0.0, 1.0, 0.0],
					[0.0, 0.0, 0.0, 1.0]],
		}
	}
}
